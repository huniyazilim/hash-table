// HashTable belirli bir genişliğe sahip olmalıdır. Bir hash fonksiyonu kullanarak
// her bir key bir slot numarasına çevirilir ve yeni node oraya kaydedilir,
// olurda farklı keyler aynı slota denk düşerse o slot üzerinde bir linked list yapısı
// oluşturacak şekilde veriler tutulur.

function HashTable(size) {
  this.buckets = Array(size);
  this.numBuckets = this.buckets.length;
}

function HashNode(key, value, next) {
  this.key = key;
  this.value = value;
  this.next = next || null;
}

HashTable.prototype.hash = function (key) {
  var total = key.split("").reduce(function (curTotal,curChar) {
    return curTotal + curChar.charCodeAt(0);
  }, 0);
  return total % this.numBuckets;
};

HashTable.prototype.insert = function (key, value) {
  var index = this.hash(key);
  if (!this.buckets[index]){
    this.buckets[index] = new HashNode(key, value);
  } else if (this.buckets[index].key === key) { // ilk öğe mevcut bir keye denk geliyorsa onu güncelle.
    this.buckets[index].value = value;
  } else {
    var currentNode = this.buckets[index];
    // aynı index üzerinde bir veya birden fazla hashNode olma ihtimaline karşın
    // var olan tüm listenin sonuna kadar git.
    while(currentNode.next) { // linked list içinde mevcut bir key ile ilgili veri geldi ise, güncelle.
      if (currentNode.next.key === key){
        currentNode.next.value = value;
      }
      currentNode = currentNode.next;
    }
    currentNode.next = new HashNode(key, value);
  }
};

HashTable.prototype.get = function (key) {
  var index = this.hash(key);
  if (!this.buckets[index]) { // böyle bir kayıt yok!
    return null;
  }

  var currentNode = this.buckets[index];

  while(currentNode){
    if (currentNode.key === key){
      return currentNode.value;
    }
    currentNode = currentNode.next;
  }
  return null;
};

HashTable.prototype.retreiveAll = function () {
  var arr = [];

  this.buckets.forEach(function (item) {
    if (item){
      var currentNode = item;
      while (currentNode) {
        arr.push(currentNode);
        currentNode = currentNode.next;
      }
    }
  });

  return arr;
};

var myHT = new HashTable(30);

myHT.insert("Nazan", "nazan@gmail.com");
myHT.insert("Atilla", "triatieli@gmail.com");
myHT.insert("Naazn", "kazım@mail.ru");
myHT.insert("Nazna", "zzkazım@mail.ru");
console.log(myHT.buckets);

myHT.insert("Naazn", "nazım@mail.ru");
console.log(myHT.buckets);

myHT.insert("Nazan", "nazan78@gmail.com");
console.log(myHT.buckets);

console.log(myHT.get("Nazan"));
console.log(myHT.get("Nazna"));
console.log(myHT.get("Atilla"));
console.log(myHT.get("Nadşr"));

console.log(myHT.retreiveAll());
